$(function () {
	var home_bg = 'url(' + $("#home-bg").attr('src') + ')';	

	$('.content-wrapper').css('min-height',  $(document).outerHeight());

	$(".hero-wrapper").css('background-image', home_bg);
	$(".hero-wrapper").css('background-position', 'top center');
	$(".hero-wrapper").css('background-repeat', 'repeat-x');

	$("footer").removeClass('footer');
	$("footer").css('background-image', home_bg);
	$("footer").css('background-position', '50% center');
	$("footer").css('background-repeat', 'repeat-x');
	// We make all our pages of a large minimum height
	$("#home-bg").hide();
});

<?php
/*
Template Name: Works
*/
?>

<?php
	global $post; 
	$my_wp_query = new WP_Query();
	$all_wp_pages = $my_wp_query->query(array('post_type' => 'page', 'post_parent' => $post->ID)); 
?>  


<?php get_header(); ?>



<div class="content-wrapper">

<div class="container_12">
	<div class="grid_12">
		<h1>Projects and Work</h1>
		<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Perferendis, omnis, tempora, non cum voluptas laboriosam harum rerum animi doloremque repellat eos odio officia voluptates ut reiciendis rem sint maiores illum.</p>
	</div>

	<?php $children = get_page_children(intval($post->ID), $all_wp_pages); ?>
	<?php foreach ($children as $child): ?>

	<a href="<?php echo get_permalink($child->ID); ?>"
		class="grid_6 hero-item" style="margin-bottom:20px">
                <?php echo get_the_post_thumbnail($child->ID, 'full'); ?>
		<span class="hero-info vevent"  id="hcalendar-Flat-is-the-New-Black">
			<span class="summary title">
				<?php echo $child->post_title; ?>
			</span>
			<time datetime="2013-06-08" class="dtstart">
				<div class="data-item date">8th June 2013</div>
				<div class="data-item time">4PM-5PM</div>
			</time>
			<span class="location data-item">ECS-304</span>
			<span class="description data-item">
				Lorem ipsum dolor sit amet, consectetur adipisicing elit.
			</span>
		</span>
	</a>
	<?php endforeach; ?>	
</div>

</div>

<?php get_footer(); ?>

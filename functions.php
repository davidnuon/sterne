<?php 
	// We're going to be using much of the bones library for this
	include_once('bones_functions.php'); 

	add_action('after_setup_theme','sigweb_init', 20);

	// Startup functions for the sigweb Theme
	function sigweb_init() {
	    add_action('wp_head', 'sigweb_styles', 1);

		 register_sidebar(array(
                        'name'=> "Call to Action",
                        'id' => 'call-to-action',
                        'before_widget' => '<div class="grid_4 call-to-action">',
                        'after_widget' => '</div>',
                        'before_title' => '<h2>',
                        'after_title' => '</h2>'
                ));


		 register_sidebar(array(
                        'name'=> "Features",
                        'id' => 'features',
                        'before_widget' => '<div class="grid_6 call-to-action">',
                        'after_widget' => '</div>',
                        'before_title' => '<h2>',
                        'after_title' => '</h2>'
		));
	}


	function sigweb_styles() {
		wp_register_style('960', get_stylesheet_directory_uri() . '/sigweb/css/960.css', array(), '', 'all' );	
		wp_register_style('960-r', get_stylesheet_directory_uri() . '/sigweb/css/960-responsive.css', array(), '', 'all' );	
		wp_register_style('main', get_stylesheet_directory_uri() . '/sigweb/css/main.css', array(), '', 'all');


		wp_enqueue_style('960');
		wp_enqueue_style('960-r');
		wp_enqueue_style('main');

	}

	function style_string($arr) {
		$out = 'style="';

		foreach ($arr as $key => $value) {
			$out .= $key . ':' . $value . ';';
		}

		$out .= '"';

		return $out;
	}

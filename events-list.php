<?php
/*
Template Name: Events
*/
?>


<?php
	global $post; 
	$my_wp_query = new WP_Query();
	$all_wp_pages = $my_wp_query->query(array('post_type' => 'page', 'post_parent' => $post->ID)); 
?>

<?php get_header(); ?>

<div class="content-wrapper">
	<div class="container_12">
		<div class="grid_12">
			<h1>Events</h1>
		<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Perferendis, et, quam, eveniet, quibusdam soluta quae excepturi maxime minima aut quo velit odit veniam magnam facilis fugit inventore earum nemo enim.</p>
		</div>
	</div>

	<div class="container_12">
		<div class="grid_12">
			<h2>
				<span class="term">Fall</span>
				<span class="year">2013</span>
			</h2>
		</div>
	</div>


	<?php $children = get_page_children(intval($post->ID), $all_wp_pages); ?>
	<?php $children = array_reverse($children); ?>
	<?php foreach ($children as $child): ?>
<div class="container_12">
	<a href="<?php echo get_permalink($child->ID); ?>" 
	  class="grid_6" style="margin-bottom:20px;">
                <?php echo get_the_post_thumbnail($child->ID, 'full'); ?>
	</a>
	<div class="grid_6 hero-item">
		<span class="hero-info vevent"  id="hcalendar-Flat-is-the-New-Black">
			<span class="summary title">
				<?php echo $child->post_title; ?>
			</span>
			<time datetime="2013-06-08" class="dtstart">
				<div class="data-item date"><?php echo get_field('date', $child->ID); ?></div>
				<div class="data-item time"><?php echo get_field('time', $child->ID); ?></div>
			</time>
			<span class="location data-item"><?php echo get_field('location', $child->ID); ?></span>
			<span class="description data-item">
				<?php echo get_field('description', $child->ID);  ?>
			</span>
		</span>
	</div>
</div>
	<?php endforeach; ?>	

</div> <!-- /Content Wrapper -->

<?php get_footer(); ?>
